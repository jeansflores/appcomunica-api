source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '~> 2.6.2'

gem 'rails', '6.0.0'

gem 'sass-rails', '~> 5'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.7'

gem 'active_storage_validations', '~> 0.7.1'
gem 'activerecord-multi-tenant', '~> 0.9.0'
gem 'aws-sdk-s3', '~> 1.36', require: false
gem 'bcrypt', '~> 3.1.7'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'cancancan', '~> 3.0'
gem 'carrierwave', '>= 2.0.0.rc', '< 3.0'
gem 'cocoon', '~> 1.2'
gem 'devise', '~> 4.7.0'
gem 'dotenv-rails', '~> 2.6'
gem "fog-aws"
gem 'has_scope', '~> 0.7.2'
gem 'jsonapi-resources', '~> 0.9.6'
gem 'jwt', '~> 2.1'
gem 'mini_magick', '~> 4.9'
gem 'onesignal', '~> 0.3.0'
gem 'pagy', '~> 3.2'
gem 'pg', '>= 0.18', '< 2.0'
gem 'pundit', '~> 2.0'
gem 'rack-cors', '~> 1.0.3'
gem 'responders', '~> 3.0.0'
gem 'translate_enum', '~> 0.1', require: 'translate_enum/active_record'
gem 'webpacker', '>= 4.0.x'
gem 'whenever', '~> 0.11.0'

group :development, :test do
  gem 'better_errors', '~> 2.5'
  gem 'binding_of_caller', '~> 0.8.0'
  gem 'faker', '~> 1.9'
  gem 'pry-byebug', '~> 3.7'
  gem 'pry-rails', '~> 0.3'
  gem 'puma', '~> 3.11'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'spring'
  gem 'web-console', github: 'rails/web-console'
end

group :production do
  gem 'passenger', '~> 6.0'
end
