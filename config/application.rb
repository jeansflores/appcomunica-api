require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AppcomunicaApi
  class Application < Rails::Application
    config.load_defaults 6.0

    config.autoload_paths += %W(
      #{config.root}/lib/appcomunica-admin/app/models
      #{config.root}/lib/appcomunica-admin/app/models/concerns
      #{config.root}/lib/appcomunica-admin/app/uploaders
      #{config.root}/lib/appcomunica-admin/app/services
      #{config.root}/lib/appcomunica-admin/app/mailers
    )

    config.time_zone = 'Brasilia'

    config.action_mailer.delivery_method = :smtp
    config.action_mailer.perform_deliveries = true
    config.action_mailer.raise_delivery_errors = true
    config.action_mailer.default charset: 'utf-8'

    config.action_mailer.smtp_settings = {
      user_name: ENV['MAIL_USERNAME'],
      password: ENV['MAIL_PASSWORD'],
      address: ENV['MAIL_ADDRESS'],
      domain: ENV['MAIL_DOMAIN'],
      port: ENV['MAIL_PORT'],
      authentication: :plain,
      enable_starttls_auto: true
    }

    config.generators.system_tests = nil

    config.i18n.load_path += Dir[
      "#{config.root}/config/locales/**/*.yml",
      "#{config.root}/lib/appcomunica-admin/config/locales/**/*.yml"
    ]

    config.i18n.default_locale = 'pt-BR'

    config.active_record.belongs_to_required_by_default = false

    if ENV['ALLOWED_ORIGINS'].present?
      config.middleware.insert_before 0, Rack::Cors do
        allow do
          origins ENV['ALLOWED_ORIGINS'].split(';')
          resource '/api/v1/*', headers: :any, methods: :any
        end
      end
    end
  end
end
