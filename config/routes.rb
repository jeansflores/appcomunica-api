Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      get 'me', to: 'users#me', as: :users
      post 'auth/token', to: 'authentications#authenticate_user'
      post 'auth/register', to: 'users/registrations#create'
      post 'auth/password/forgot', to: 'users/passwords#forgot'
      post 'auth/password/reset', to: 'users/passwords#reset'
      put 'auth/password/update', to: 'users/passwords#update'
      get 'researches/show-by-user', to: 'researches#show_by_user'
      get 'researches/progress-by-user', to: 'researches#progress_by_user'
      get 'raffles/remaining-votes-by-user', to: 'raffles#remaining_votes_by_user'
      get 'promotions/remaining-votes-by-user', to: 'promotions#remaining_votes_by_user'

      jsonapi_resources :addresses, only: [:index, :show]
      jsonapi_resources :ads, only: [:index, :show]
      jsonapi_resources :categories, only: [:index, :show]
      jsonapi_resources :cities, only: [:index, :show]
      jsonapi_resources :companies, only: [:index, :show]
      jsonapi_resources :contacts, only: [:index, :show]
      jsonapi_resources :countries, only: [:index, :show]
      jsonapi_resources :coupons, only: [:index, :show, :create]
      jsonapi_resources :events, only: [:index, :show]
      jsonapi_resources :institutes, only: [:index, :show]
      jsonapi_resources :items, only: [:index, :show]
      jsonapi_resources :owners, only: [:index, :show]
      jsonapi_resources :posts, only: [:index, :show]
      jsonapi_resources :promotions, only: [:index, :show]
      jsonapi_resources :raffles, only: [:index, :show]
      jsonapi_resources :research_participants, only: [:index, :show]
      jsonapi_resources :researches, only: [:index, :show]
      jsonapi_resources :sponsors, only: [:index, :show]
      jsonapi_resources :states, only: [:index, :show]
      jsonapi_resources :tickets, only: [:index, :show, :create]
      jsonapi_resources :users, only: [:index, :show, :update]
      jsonapi_resources :votes, only: [:index, :show, :create, :update]
    end
  end
end
