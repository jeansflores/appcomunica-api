class Api::V1::CountryResource < JSONAPI::Resource
  attributes :name, :code

  has_many :states
end
