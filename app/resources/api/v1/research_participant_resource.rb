class Api::V1::ResearchParticipantResource < JSONAPI::Resource
  has_one :research
  has_one :category
  has_one :company

  has_many :votes

  filters :research_id
end
