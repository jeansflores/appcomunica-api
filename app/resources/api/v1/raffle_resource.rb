class Api::V1::RaffleResource < JSONAPI::Resource
  attributes :name, :raffled_at, :amount_available, :status, :description, :regulation, :image,
    :published_at, :minimum_votes, :single

  has_one :institute
  has_one :research
  has_one :company

  has_many :tickets

  paginator :paged

  filter :institute_id
  filter :status, default: [:opened, :closed]

  filter :name, apply: ->(records, value, _options) {
    records.order(id: :desc).where(
      "unaccent(name) ILIKE unaccent(:name)", name: "%#{value[0]}%"
    )
  }

  def image
    @model.image&.large.url
  end

  def status
    @model.translated_status
  end

  def minimum_votes
    @model.minimum_votes
  end
end
