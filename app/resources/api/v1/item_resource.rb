class Api::V1::ItemResource < JSONAPI::Resource
  attributes :name, :description, :group, :image

  has_one :institute

  filter :institute_id

  def image
    @model.image&.large.url
  end

  def group
    @model.translated_item_type
  end
end
