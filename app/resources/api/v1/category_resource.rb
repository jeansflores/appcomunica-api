class Api::V1::CategoryResource < JSONAPI::Resource
  attribute :name

  paginator :none
end
