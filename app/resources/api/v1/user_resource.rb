class Api::V1::UserResource < JSONAPI::Resource
  attributes :name, :email, :avatar, :password

  has_one :institute, optional: true

  has_many :tickets

  def avatar
    @model.avatar&.medium.url
  end
end
