class Api::V1::CompanyResource < JSONAPI::Resource
  attributes :social_name, :trade_name, :cnpj, :about, :logo

  filter :category_id

  has_one :category
  has_one :institute

  has_one :address
  has_one :contact

  has_many :promotions
  has_many :raffles

  def logo
    @model.logo? ? @model.logo&.medium.url : nil
  end
end
