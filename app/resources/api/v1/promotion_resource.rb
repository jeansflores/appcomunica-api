class Api::V1::PromotionResource < JSONAPI::Resource
  attributes :name, :expires_at, :amount_available, :status, :description, :image, :published_at, :has_coupons

  has_one :institute
  has_one :company

  has_many :coupons

  paginator :paged

  filter :institute_id
  filter :status, default: [:opened, :closed]

  filter :name, apply: ->(records, value, _options) {
    records.order(id: :desc).where(
      "unaccent(name) ILIKE unaccent(:name)", name: "%#{value[0]}%"
    )
  }

  def image
    @model.image&.large.url
  end

  def status
    @model.translated_status
  end
end
