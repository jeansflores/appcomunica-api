class Api::V1::ContactResource < JSONAPI::Resource
  attributes :phone, :email, :whatsapp_phone

  has_one :owner, polymorphic: true
end
