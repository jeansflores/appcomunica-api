class Api::V1::PostResource < JSONAPI::Resource
  attributes :title, :content, :image, :published_at

  has_one :institute

  filter :institute_id
  filter :status, default: [:published]

  paginator :paged

  filter :name, apply: ->(records, value, _options) {
    records.order(id: :desc).where(
      "unaccent(title) ILIKE unaccent(:title)", title: "%#{value[0]}%"
    )
  }

  def image
    @model.image&.large.url
  end
end
