class Api::V1::ResearchResource < JSONAPI::Resource
  attributes :name, :start_at, :end_at, :status, :banner, :logo, :published_at, :regulation,
    :description, :total_participants

  filter :status, default: :opened

  filters :institute_id, :research_participant_id

  has_one :institute

  has_many :research_participants
  has_many :sponsors

  def logo
    @model.logo&.medium.url
  end

  def banner
    @model.banner&.large.url
  end
end
