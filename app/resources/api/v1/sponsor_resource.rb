class Api::V1::SponsorResource < JSONAPI::Resource
  attributes :name, :logo

  has_one :research

  def logo
    @model.logo&.medium.url
  end
end
