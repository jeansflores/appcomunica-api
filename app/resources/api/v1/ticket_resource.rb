class Api::V1::TicketResource < JSONAPI::Resource
  attributes :raffled, :token, :user_id, :raffle_id

  has_one :user
  has_one :raffle

  filters :user_id, :raffle_id
end
