class Api::V1::CouponResource < JSONAPI::Resource
  attributes :token, :user_id, :promotion_id

  has_one :user
  has_one :promotion

  filters :user_id, :promotion_id
end
