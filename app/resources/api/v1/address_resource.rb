class Api::V1::AddressResource < JSONAPI::Resource
  attributes :zipcode, :district, :street, :number, :complement, :map_url

  has_one :city
  has_one :owner, polymorphic: true
end
