class Api::V1::EventResource < JSONAPI::Resource
  attributes :name, :map_url, :link, :start_at, :status, :published_at, :description, :image

  has_one :institute

  paginator :paged

  filter :institute_id
  filter :status, default: [:opened, :closed]

  filter :name, apply: ->(records, value, _options) {
    records.order(id: :desc).where(
      "unaccent(name) ILIKE unaccent(:name)", name: "%#{value[0]}%"
    )
  }

  def image
    @model.image&.large.url
  end
end
