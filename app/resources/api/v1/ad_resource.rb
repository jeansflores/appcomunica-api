class Api::V1::AdResource < JSONAPI::Resource
  attributes :local, :image, :expire_at, :name

  has_one :institute

  filters :institute_id, :local
  filter :non_expired, apply: ->(records, value, _options) {
    records.active
  }

  def image
    @model.image&.slim&.url
  end
end
