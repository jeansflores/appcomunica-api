class Api::V1::StateResource < JSONAPI::Resource
  attributes :name, :acronym

  has_one :country

  has_many :cities
end
