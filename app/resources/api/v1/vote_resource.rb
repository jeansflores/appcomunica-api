class Api::V1::VoteResource < JSONAPI::Resource
  attributes :rating

  has_one :research_participant
  has_one :user

  filters :user_id, :research_participant_id
end
