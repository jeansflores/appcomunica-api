class Api::V1::CityResource < JSONAPI::Resource
  attributes :name

  has_one :state

  has_many :addresses
end
