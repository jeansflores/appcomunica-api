class Api::V1::InstituteResource < JSONAPI::Resource
  attributes :social_name, :about, :cnpj, :trade_name, :accesses, :logo

  has_one :address, as: :owner
  has_one :contact, as: :owner

  has_many :companies
  has_many :events
  has_many :items
  has_many :posts
  has_many :promotions
  has_many :raffles
  has_many :researchs
  has_many :research_participants

  def logo
    @model.logo? ? @model.logo&.medium.url : nil
  end

  def accesses
    @model.accesses.map { |a| a.key.downcase }
  end
end
