class Api::V1::Users::RegistrationsController < Api::V1::Controller
  def create
    @user = ApiService.find_or_create(params, institute_id)

    if @user.errors.blank?
      render(
        status: 200,
        json: JSONAPI::ResourceSerializer.new(Api::V1::UserResource)
          .serialize_to_hash(Api::V1::UserResource.new(@user, nil)),
      )
    else
      response = []

      @user.errors.full_messages.each do |message|
        response << { message: message }
      end

      render(
        json: { errors: response },
        status: 422
      )
    end
  end
end
