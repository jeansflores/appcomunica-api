class Api::V1::Users::PasswordsController < Api::V1::Controller
  def forgot
    if params[:email].blank?
      return render json: { errors: [{ message: I18n.t('api.errors.messages.required_email'), status: 422 }] }, status: 422
    end

    user_requester = User.find_by(email: params[:email].downcase)

    if user_requester.present?
      user_requester.generate_password_token!

      Api::V1::PasswordMailer.forgot(user_requester).deliver_now

      render json: { message: I18n.t('api.messages.success.successfully_forwarded_password_change_email'), status: 200 }, status: :ok
    else
      render json: { errors: [{ message: I18n.t('api.errors.messages.email_not_found_check_and_try_again'), status: 404 }] }, status: :not_found
    end
  end

  def reset
    if params[:token].blank?
      return render json: { errors: [{ message: I18n.t('api.errors.messages.required_token'), status: 422 }] }, status: 422
    end

    if params[:password].blank?
      return render json: { errors: [{ message: I18n.t('api.errors.messages.required_password'), status: 422 }] }, status: 422
    end

    user_requester = User.find_by(reset_password_token: params[:token])

    if user_requester.present? && user_requester.password_token_valid?
      if user_requester.reset_password(params[:password])
        render json: { message: I18n.t('api.messages.success.password_changed_successfullys'), status: 200 }, status: :ok
      else
        response = []

        user_requester.errors.full_messages.each do |message|
          response << { message: message }
        end

        render(
          json: { errors: response },
          status: 422
        )
      end
    else
      render json: { errors: [{ message: I18n.t('api.errors.messages.invalid_or_expired_code'), status: 404 }] }, status: :not_found
    end
  end
end
