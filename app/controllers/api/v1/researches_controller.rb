class Api::V1::ResearchesController < Api::V1::Controller
  def show_by_user
    page = params[:page] || {}

    page[:limit] ||= 10
    page[:offset] ||= 0

    research = Research.find_by(status: :opened, institute_id: institute_id)

    unless research
      return render json: { data: nil }
    end

    research_categories = [].tap do |categories|
      research.categories.distinct.order(:name).each do |category|
        categories << { name: category.name, value: category.id }
      end
    end

    research_categories.unshift({ name: 'Todas Categorias', value: nil })

    data = ResearchParticipant.joins(:research).where(research_id: research.id)
      .then { |p| params[:category_id] ? p.where(category_id: params[:category_id]) : p }
      .then do |p|
        idsWithVote = ResearchParticipant.joins(:votes).where(votes: { user_id: params[:user_id] }).ids

        if !params.key?(:reviewd)
          p
        elsif params[:reviewd].to_bool
          p.where(id: idsWithVote)
        else
          p.where.not(id: idsWithVote)
        end
      end
      .then do |p|
        if params.key?(:trade_name)
          p.joins(:company).
            where("unaccent(companies.trade_name) ILIKE unaccent(:trade_name)", trade_name: "%#{params[:trade_name]}%")
        else
          p
        end
      end.joins(:company).order('companies.trade_name')
      .offset(page[:offset])
      .limit(page[:limit])
      .then { |p| p.to_json(include: [:votes, :category, { company: { methods: [:logo_url, :address] } }]) }
      .then { |p| JSON.parse(p) }
      .then do |p|
        p.map do |i|
          i['votes'].delete_if { |v| v['user_id'] != params[:user_id].to_i }
          i['vote'] = i['votes'].first
          i.delete('votes')
          i
        end
      end
      .then do |p|
        {
          research: research.attributes.merge(
            banner: research.banner&.large.url,
            logo: research.logo&.medium.url,
            regulation: research.regulation,
            description: research.description,
            categories: research_categories,
            participants: p,
            sponsors: extract_sponsors(research),
          )
        }
      end
      .then { |r| r.deep_transform_keys! { |k| k.to_s.camelize(:lower) } }

    render json: data
  end

  def progress_by_user
    research = Research.find_by(status: :opened, institute_id: institute_id)

    unless research
      return render json: { data: nil }
    end

    total_participants = research.total_participants
    total_reviews = research.votes.where(user_id: params.fetch(:user_id))

    progress_categories = [].tap do |categories|
      research.categories.distinct.each do |category|
        total_votes = total_reviews.select { |vote| vote.category.id == category.id }.count
        categories << { name: category.name, votes: total_votes }
      end
    end

    render(
      json: {
        progress: {
          totalParticipants: total_participants,
          totalReviews: total_reviews.count,
          ratioCompleted: (total_reviews.count.to_f / total_participants.to_f),
          progressCategories: progress_categories
        }
      }
    )
  end

  private

  def extract_sponsors(research)
    [].tap do |sponsors|
      research.sponsors.distinct.each do |sponsor|
        sponsors << { id: sponsor.id, name: sponsor.name, logo: sponsor.logo.medium.url }
      end
    end
  end
end
