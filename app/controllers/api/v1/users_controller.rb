class Api::V1::UsersController < Api::V1::Controller
  def me
    render(
      json: JSONAPI::ResourceSerializer.new(Api::V1::UserResource).serialize_to_hash(Api::V1::UserResource.new(@current_user, nil)),
      status: 200
    )
  end

  def update
    user = User.find(params[:id])

    if user.present? && current_user.id == params[:id].to_i
      super
    else
      render json: { errors: [{ message: 'Usuário inválido', status: 401 }] }, status: :unauthorized
    end
  end
end
