class Api::V1::RafflesController < Api::V1::Controller
  def remaining_votes_by_user
    user = User.find(params[:user_id])
    raffle = Raffle.find(params[:raffle_id])
    remaining_votes = raffle.minimum_votes - user.votes.from_research(raffle.research).size

    render json: remaining_votes.negative? ? 0 : remaining_votes
  end
end
