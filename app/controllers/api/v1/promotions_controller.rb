class Api::V1::PromotionsController < Api::V1::Controller
  def remaining_votes_by_user
    user = User.find(params[:user_id])
    promotion = Promotion.find(params[:promotion_id])
    remaining_votes = promotion.minimum_votes - user.votes.from_research(promotion.research).size

    render json: remaining_votes.negative? ? 0 : remaining_votes
  end
end
