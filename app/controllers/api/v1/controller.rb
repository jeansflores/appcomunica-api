class Api::V1::Controller < ActionController::Base
  include JSONAPI::ActsAsResourceController
  respond_to :json
  protect_from_forgery with: :null_session
  before_action :authenticate_request!, unless: :validate_request?

  before_action do
    ActiveStorage::Current.host = request.base_url
  end

  attr_reader :current_user, :institute_id

  protected

  def authenticate_request!
    unless user_id_in_token? && !JsonWebToken.valid_payload?(auth_token)
      render json: { errors: [{ message: I18n.t('api.errors.messages.not_authenticated'), status: 401 }] }, status: :unauthorized
      return
    end

    @current_user = User.find(auth_token[:user_id])
  rescue JWT::VerificationError, JWT::DecodeError
    render json: { errors: [{ message: I18n.t('api.errors.messages.not_authenticated'), status: 401 }] }, status: :unauthorized
  end

  private

  def http_token
    @http_token ||= if request.headers['Authorization'].present?
      request.headers['Authorization'].split(' ').last
    end
  end

  def auth_token
    @auth_token ||= JsonWebToken.decode(http_token)
  end

  def institute_id
    @institute_id ||= request.headers['Institute-Id']
  end

  def user_id_in_token?
    http_token && auth_token && auth_token[:user_id].to_i
  end

  def validate_request?
    request_paths = [ "/api/v1/auth/token", "/api/v1/auth/register", "/api/v1/auth/password/forgot",
      "/api/v1/auth/password/reset", "/api/v1/auth/password/update" ]

    request.headers['REQUEST_METHOD'] == "POST" &&
    request.headers['CONTENT_TYPE'] == "application/json" &&
    request.headers['REQUEST_URI'].in?(request_paths)
  end
end
