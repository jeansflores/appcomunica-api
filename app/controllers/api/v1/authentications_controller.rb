class Api::V1::AuthenticationsController < Api::V1::Controller
  protect_from_forgery with: :null_session

  def authenticate_user
    if institute_id.blank?
      return render json: {
        status: :forbidden,
        errors: [
          {
            message: I18n.t('api.errors.messages.you_are_not_allowed_in_this_action'),
            status: 403
          }
        ]
      }
    end

    user = User.find_by(email: params[:email], institute_id: institute_id)

    if user.present? && (user.valid_password?(params[:password]) || valid_social_id?(user))
      if !params[:device_token].nil? && !params[:device_token][:pushToken].nil? && !params[:device_token][:userId].nil?
        Device.find_or_create_by(
          onesignal_token: params[:device_token][:userId],
          token: params[:device_token][:pushToken],
          user_id: user.id
        )
      end

      render json: payload(user)
    else
      render json: { errors: [{ message: I18n.t('api.errors.messages.invalid_email_or_password'), status: 401 }] }, status: :unauthorized
    end
  end

  private

  def valid_social_id?(user)
    if params[:google_id].present?
      user.google_id == params[:google_id]
    elsif params[:facebook_id].present?
      user.facebook_id == params[:facebook_id]
    else
      false
    end
  end

  def payload(user)
    return nil unless user&.id

    {
      token: JsonWebToken.encode(
        { user_id: user.id, iat: DateTime.current.to_i }
      ),
      user: {
        id: user.id
      }
    }
  end
end
