class Api::V1::PasswordMailer < ApplicationMailer
  default template_path: "mailers/#{self.name.underscore}"

  def forgot(user)
    @user = user
    @institute = user.institute

    mail(to: @user.email, subject: 'Alteração de senha')
  end
end
