class ApiService
  def self.find_or_create(params, institute_id)
    user =
      if params[:registration].key?("google_id") || params[:registration].key?("facebook_id")
        User.find_by(email: params[:registration][:email], institute_id: institute_id)
      else
        nil
      end

    if user.present?
      if params[:registration][:google_id]
        user.google_id.nil? ? user.update(google_id: params[:registration][:google_id]) : ''
      elsif params[:registration][:facebook_id]
        user.facebook_id.nil? ? user.update(facebook_id:params[:registration][:facebook_id]) : ''
      end
    else
      user = User.new(sign_up_params(params).merge(institute_id: institute_id))
    end

    if user.avatar.blank? && params[:registration][:avatar]
      user.avatar = download_image(params[:registration][:avatar])
      user.save
    end

    user.save unless user.persisted?

    user
  end

  def self.sign_up_params(params)
    params.require(:registration).
      permit(:email, :name, :password, :institute_id, :facebook_id, :google_id, :origin, :avatar)
  end

  def self.download_image(url)
    image = MiniMagick::Image.open(url, 'jpg')

    ImageProcessing::MiniMagick.source(image).convert!("jpg")
  end
end
